<div class="sidebar">
	<h3 class="sidebar_title">Recent Work</h3>
	
	<?php
		$the_query = new WP_Query('category_name=work');
		while ( $the_query->have_posts() ) : $the_query->the_post();
		$post_id = get_the_ID();
		$source = get_post_custom_values('src', $post_id);
		$width = get_post_custom_values('width', $post_id);
		$height = get_post_custom_values('height', $post_id);
	?>
		<div class="item">
			<h4>
				<a href="<?php the_permalink(); ?>">
					<?php the_title(); ?>
				</a>
			</h4>
			<p class="date"><?php the_time(get_option('date_format')); ?></p>
		</div>
	<?php
		endwhile;
		// Reset Post Data
		wp_reset_postdata();
	?>
</div>
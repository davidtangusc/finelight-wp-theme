<?php
/**
 * Template Name: Contact template - 1 column
 */
?>

<?php get_header(); ?>
	<?php the_post(); ?> 
	<h2 class="page_title"><?php the_title(); ?></h2>
	<div class="page_description" style="width:600px;">
		<?php the_content(); ?>
	</div>
		
	<?php
		$the_query = new WP_Query('category_name=locations');
		while ( $the_query->have_posts() ) : $the_query->the_post();
		$post_id = get_the_ID();
		$source = get_post_custom_values('src', $post_id);
		$width = get_post_custom_values('width', $post_id);
		$height = get_post_custom_values('height', $post_id);
		$phone = get_post_custom_values('phone', $post_id);
	?>
		<div class="city">
			<img src="<?php echo site_url(); ?>/images/<?php echo $source[0]; ?>" width="<?php echo $width[0]; ?>" height="<?php echo $height[0]; ?>" alt="<?php the_title(); ?>" />
			<div class="info">
				<h3><?php the_title(); ?></h3>
				<p class="phone"><?php echo $phone[0]; ?></p>
			</div>
			<div class="clear"></div>
		</div>
	<?php
		endwhile;
		// Reset Post Data
		wp_reset_postdata();
	?>
	<?php edit_post_link('Edit this entry','','.'); ?>
<?php get_footer(); ?>
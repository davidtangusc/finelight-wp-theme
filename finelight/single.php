<?php get_header(); ?>

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

	<?php 
		$post_id = get_the_ID();
		$media = get_post_custom_values('media-embed-code', $post_id);
		$media = $media[0];
	?>

			<div class="main">
				<h2 class="page_title"><?php the_title(); ?></h2>
				<div class="media"><?php echo $media; ?></div>
				<div class="media-description"><?php the_content(); ?></div>
				<?php edit_post_link('Edit this entry','','.'); ?>
			</div><!-- .main -->

	<?php endwhile; endif; ?>
	
<?php get_sidebar(); ?>

<?php get_footer(); ?>
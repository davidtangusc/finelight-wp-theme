<?php
/**
 * Template Name: Team template - 2 columns
 */
?>

<?php get_header(); ?>
<div class="main">
	<?php the_post(); ?> 
	<h2 class="page_title"><?php the_title(); ?></h2>
	<div class="page_description" style="width:600px;">
		<?php the_content(); ?>
	</div>
		
	<?php
		$the_query = new WP_Query('category_name=team');
		while ( $the_query->have_posts() ) : $the_query->the_post();
		$post_id = get_the_ID();
		$source = get_post_custom_values('src', $post_id);
		$width = get_post_custom_values('width', $post_id);
		$height = get_post_custom_values('height', $post_id);
	?>
		<div class="team_member">
			<img src="<?php echo site_url(); ?>/images/<?php echo $source[0]; ?>" width="<?php echo $width[0]; ?>" height="<?php echo $height[0]; ?>" alt="<?php the_title(); ?>" />
			<h3><?php the_title(); ?></h3>
			<p><?php the_content(); ?></p>	
			<div class="clear"></div>
		</div>
	<?php
		endwhile;
		// Reset Post Data
		wp_reset_postdata();
	?>
	<?php edit_post_link('Edit this entry','','.'); ?>
</div><!-- .main -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
	<div class="footer">
		<div class="icons">
			<a href="#" target="_blank" class="facebook"></a>
			<a href="#" target="_blank" class="twitter"></a>
			<a href="#" target="_blank" class="vimeo"></a>
			<a href="#" target="_blank" class="youtube"></a>
		</div>
	</div>
</div><!-- .container -->

<?php wp_footer(); ?>


<script src="<?php bloginfo('stylesheet_directory'); ?>/plugins/jscrollpane/jquery.mousewheel.js"></script>
<script src="<?php bloginfo('stylesheet_directory'); ?>/plugins/jscrollpane/jquery.jscrollpane.min.js"></script>
<script src="<?php bloginfo('stylesheet_directory'); ?>/plugins/fancyBox/source/jquery.fancybox.pack.js"></script>

<!-- this is where we put our custom functions -->
<script src="<?php bloginfo('stylesheet_directory'); ?>/js/main.js"></script>


<!-- Asynchronous google analytics; this is the official snippet.
	 Replace UA-XXXXXX-XX with your site's ID and uncomment to enable.
	 
<script>

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-XXXXXX-XX']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
-->
	
</body>

</html>

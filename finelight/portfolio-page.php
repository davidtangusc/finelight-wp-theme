<?php
/**
 * Template Name: Portfolio template - 2 columns
 */
?>

<?php get_header(); ?>
	<?php the_post(); ?> 
	<h2 class="page_title"><?php the_title(); ?></h2>
	<div class="portfolio-filter" id="portfolio-filter">
		<a href="<?php echo site_url(); ?>/portfolio" <?php echo ($_GET['filter'] != 'film' &&  $_GET['filter'] != 'music' ? 'class="selected-filter"' : ''); ?>>All</a>
		<a href="<?php echo site_url(); ?>/portfolio?filter=film" <?php echo ($_GET['filter'] == 'film' ? 'class="selected-filter"' : ''); ?>>Film</a>
		<a href="<?php echo site_url(); ?>/portfolio?filter=music" <?php echo ($_GET['filter'] == 'music' ? 'class="selected-filter"' : ''); ?>>Music</a>
	</div>
	<div class="portfolio-container" id="portfolio-container">
	<?php

		if(isset($_GET['filter'])) {
			$filter = trim($_GET['filter']);

			switch ($filter) {
				case 'film':
					$category_name = 'film';
					break;
				case 'music':
					$category_name = 'music';
					break;
				default:
					$category_name = 'work';
					break;
			}
		}
		else {
			$category_name = 'work';
		}

		$the_query = new WP_Query("category_name=$category_name");
		while ( $the_query->have_posts() ) : $the_query->the_post();
		$post_id = get_the_ID();
		$source = get_post_custom_values('src', $post_id);
		//$media = get_post_custom_values('media-embed-code', $post_id);
		//$width = get_post_custom_values('width', $post_id);
		//$height = get_post_custom_values('height', $post_id);
	?>
	
		<div class="portfolio_item">
			<a href="#fancybox-modal" class="ajax-fancybox" rel="<?php the_permalink(); ?>">
				<img src="<?php echo site_url(); ?>/images/<?php echo $source[0]; ?>" width="<?php echo $width[0]; ?>" height="<?php echo $height[0]; ?>" alt="<?php the_title(); ?>" />
			</a>
			<div class="portfolio_info item">
				<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
				<p class="date"><?php the_time(get_option('date_format')); ?></p>
			</div>
		</div>

	<?php
		endwhile;
		// Reset Post Data
		wp_reset_postdata();
	?>
	<div id="fancybox-modal"></div>
	</div>
	<?php edit_post_link('Edit this entry','','.'); ?>

<?php get_footer(); ?>
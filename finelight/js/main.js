$(function() {

	function swapImageSrc(val1, val2) {
		var src = this.src;
		var newSrc = src.replace(val1, val2);
		this.src = newSrc;
	}


	$('#logo')
		.on('mouseover', function() {
			swapImageSrc.call(this, 'logo-lightOff.png', 'logo-lightOn.png');

		})
		.on('mouseout', function() {
			swapImageSrc.call(this, 'logo-lightOn.png', 'logo-lightOff.png');
		});

	$('#portfolio-container').jScrollPane({
		hideFocus: true
	});


	$('.ajax-fancybox').on('mouseover', function(e) {
		var url = this.rel;
		console.log(url)
		$('#fancybox-modal').load(url + ' iframe');

		$(this).fancybox({
			width: '500px',
			height: '281px',
			autoSize: false
		});

	});
});
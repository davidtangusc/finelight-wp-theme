<?php
/**
 * Template Name: Home template - 1 column
 */
?>

<?php get_header(); ?>
	<?php 
		$post_id = get_the_ID();
		the_post();
	 	$media_embed_code = get_post_custom_values('media-embed-code', $post_id);
	 	$media_embed_code = $media_embed_code[0];

	 ?> 

	<div class="home-video">
		<?php echo $media_embed_code; ?>
	</div>
			
	<div class="featured">
		<?php the_content(); ?>
	</div>

	<?php edit_post_link('Edit this entry','','.'); ?>

<?php get_footer(); ?>